package com.amcn.downloads;

import android.database.Observable;

class DownloadObservable extends Observable<DownloadObserver> {

    void onDownloadStarted(String assetId) {
        for (int i = 0; i < mObservers.size(); i++) {
            mObservers.get(i).onDownloadStarted(assetId);
        }
    }

    void onDownloadProgressUpdate(String assetId, long currentSize, long totalSize) {
        for (int i = 0; i < mObservers.size(); i++) {
            mObservers.get(i).onDownloadProgressUpdate(assetId, currentSize, totalSize);
        }
    }

    void onDownloadCompleted(String assetId) {
        for (int i = 0; i < mObservers.size(); i++) {
            mObservers.get(i).onDownloadCompleted(assetId);
        }
    }

    void onDownloadError(String assetId) {
        for (int i = 0; i < mObservers.size(); i++) {
            mObservers.get(i).onDownloadError(assetId);
        }
    }

    void onDownloadDeleted(String assetId) {
        for (int i=0; i<mObservers.size(); i++) {
            mObservers.get(i).onDownloadDeleted(assetId);
        }
    }
}
