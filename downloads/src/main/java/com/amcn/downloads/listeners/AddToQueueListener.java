package com.amcn.downloads.listeners;

@SuppressWarnings("unused")
public interface AddToQueueListener {

    void onSuccess(String assetId);

    void onFailure(String assetId);
}
