package com.amcn.downloads;

import androidx.annotation.NonNull;

import com.amcn.downloads.model.DownloadingItem;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

class DownloadingCache {

    private Map<String, DownloadingItem> cache = new LinkedHashMap<>();
    private DownloadingItem currentDownloading = null;

    void updateAssetStatus(@NonNull DownloadingItem item) {
        if (item.getDownloadStatus() == DownloadStatus.DOWNLOADING) {
            currentDownloading = item;
        }
        cache.put(item.getAssetId(), item);
        clearCacheIfNeeded();
    }

    void removeAsset(String assetId) {
        cache.remove(assetId);
        clearCacheIfNeeded();
    }

    DownloadingItem getAssetStatus(String assetId) {
        return cache.get(assetId);
    }

    int getTotalCount() {
        return cache.size();
    }

    DownloadingItem getCurrentDownloading() {
        return currentDownloading;
    }

    int getFinishedCount() {
        int finishedCount = 0;
        for (DownloadingItem item : cache.values()) {
            if (item.getDownloadStatus() == DownloadStatus.DOWNLOADED || item.getDownloadStatus() == DownloadStatus.ERROR) {
                finishedCount++;
            }
        }
        return finishedCount;
    }

    Collection<DownloadingItem> getCache() {
        return cache.values();
    }

    /**
     * Clear cache in case if all downloads is finished
     */
    private void clearCacheIfNeeded() {
        if (getFinishedCount() == getTotalCount()) {
            cache.clear();
            currentDownloading = null;
        }
    }
}
