package com.amcn.downloads;

import androidx.annotation.IntDef;

import static com.amcn.downloads.DownloadStatus.DOWNLOADED;
import static com.amcn.downloads.DownloadStatus.DOWNLOADING;
import static com.amcn.downloads.DownloadStatus.ERROR;
import static com.amcn.downloads.DownloadStatus.NOT_EXISTS;

@IntDef({NOT_EXISTS, DOWNLOADING, DOWNLOADED, ERROR})
public @interface DownloadStatus {

    int NOT_EXISTS = 0;
    int DOWNLOADING = 1;
    int DOWNLOADED = 2;
    int ERROR = 3;
}
