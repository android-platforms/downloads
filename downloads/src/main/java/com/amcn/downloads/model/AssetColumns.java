package com.amcn.downloads.model;

public class AssetColumns {
    public static final String _ID = AssetColumnsImpl._ID;
    public static final String DOWNLOAD_STATUS = AssetColumnsImpl.DOWNLOAD_STATUS;
    public static final String METADATA = AssetColumnsImpl.METADATA;
    public static final String CURRENT_SIZE = AssetColumnsImpl.CURRENT_SIZE;
    public static final String EXPECTED_SIZE = AssetColumnsImpl.EXPECTED_SIZE;
    public static final String CREATION_TIME = AssetColumnsImpl.CREATION_TIME;

    private AssetColumns() {
    }
}