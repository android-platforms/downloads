package com.amcn.downloads.model;

import com.amcn.downloads.DownloadStatus;

public class DownloadingItem {

    private String assetId;
    private long currentSize;
    private long totalSize;
    private String metadata;
    private int downloadStatus;

    public String getAssetId() {
        return assetId;
    }

    public void setAssetId(String assetId) {
        this.assetId = assetId;
    }

    public long getCurrentSize() {
        return currentSize;
    }

    public void setCurrentSize(long currentSize) {
        this.currentSize = currentSize;
    }

    public long getTotalSize() {
        return totalSize;
    }

    public void setTotalSize(long totalSize) {
        this.totalSize = totalSize;
    }

    public String getMetadata() {
        return metadata;
    }

    public void setMetadata(String metadata) {
        this.metadata = metadata;
    }

    public @DownloadStatus
    int getDownloadStatus() {
        return downloadStatus;
    }

    public void setDownloadStatus(@DownloadStatus int downloadStatus) {
        this.downloadStatus = downloadStatus;
    }
}
