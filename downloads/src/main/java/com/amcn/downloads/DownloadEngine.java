package com.amcn.downloads;

import android.content.Context;
import android.net.Uri;

import com.amcn.downloads.listeners.AddToQueueListener;
import com.amcn.downloads.model.DownloadingItem;

import java.net.URL;

@SuppressWarnings("unused")
public abstract class DownloadEngine {

    private static DownloadEngine engine;

    public static DownloadEngine getInstance(Context applicationContext) {
        if (engine == null) {
            engine = new DownloadEngineImpl(applicationContext);
        }
        return engine;
    }

    public abstract void startEngine(URL url, String userId, String publicKey, String privateKey);

    public abstract void stopEngine();

    public abstract boolean isAuthorized();

    public abstract Uri getContentURI();

    public abstract Uri getExpiredURI();

    public abstract Uri getQueueURI();

    public abstract @DownloadStatus
    int getAssetDownloadStatus(String assetId);

    public abstract DownloadingItem getDownloadingItem(String assetId);

    public abstract int getDownloadingQueueSize();

    public abstract int getDownloadingFinishedCount();

    public abstract DownloadingItem getCurrentDownloadingItem();

    public abstract String getAssetUri(String assetId);

    public abstract void onResume();

    public abstract void onPause();

    public abstract void addDownloadObserver(DownloadObserver observer);

    public abstract void removeDownloadObserver(DownloadObserver observer);

    public abstract void downloadDash(String url, String pid, String metadata, long expirationDate, AddToQueueListener listener);

    public abstract void deleteAsset(String assetId);

    public abstract boolean isExpired(String assetId);

    public abstract boolean isDiskCapacityOk();

    public abstract boolean isDownloadLimitOk();

    public abstract boolean isNetworkStatusOk();

    public abstract boolean isPowerStatusOK();

    public abstract boolean isDownloadAvailable();
}
