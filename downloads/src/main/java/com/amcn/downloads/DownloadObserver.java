package com.amcn.downloads;

public class DownloadObserver {

    public void onDownloadStarted(String assetId) {
    }

    public void onDownloadProgressUpdate(String assetId, long currentSize, long totalSize) {
    }

    public void onDownloadCompleted(String assetId) {
    }

    public void onDownloadError(String assetId) {
    }

    public void onDownloadDeleted(String assetId) {
    }
}
