package com.amcn.downloads.model;

public class AssetColumnsImpl {

    public static final String _ID = "STUB";
    public static final String DOWNLOAD_STATUS = "STUB";
    public static final String METADATA = "STUB";
    public static final String CURRENT_SIZE = "STUB";
    public static final String EXPECTED_SIZE = "STUB";
    public static final String CREATION_TIME = "STUB";

    private AssetColumnsImpl() {
    }
}
