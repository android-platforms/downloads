package com.amcn.downloads;

import android.content.Context;
import android.net.Uri;

import com.amcn.downloads.listeners.AddToQueueListener;
import com.amcn.downloads.model.DownloadingItem;

import java.net.URL;

class DownloadEngineImpl extends DownloadEngine {

    DownloadEngineImpl(Context applicationContext) {
    }

    @Override
    public void startEngine(URL url, String userId, String publicKey, String privateKey) {
        // STUB
    }

    @Override
    public void stopEngine() {
        // STUB
    }

    @Override
    public boolean isAuthorized() {
        // STUB
        return false;
    }

    @Override
    public Uri getContentURI() {
        // STUB
        return null;
    }

    @Override
    public Uri getExpiredURI() {
        // STUB
        return null;
    }

    @Override
    public Uri getQueueURI() {
        // STUB
        return null;
    }

    @Override
    public @DownloadStatus
    int getAssetDownloadStatus(String assetId) {
        // STUB
        return DownloadStatus.NOT_EXISTS;
    }

    @Override
    public DownloadingItem getDownloadingItem(String assetId) {
        // STUB
        return null;
    }

    @Override
    public int getDownloadingQueueSize() {
        // STUB
        return 0;
    }

    @Override
    public int getDownloadingFinishedCount() {
        // STUB
        return 0;
    }

    @Override
    public DownloadingItem getCurrentDownloadingItem() {
        // STUB
        return null;
    }

    @Override
    public String getAssetUri(String assetId) {
        // STUB
        return null;
    }

    @Override
    public void onResume() {
        // STUB
    }

    @Override
    public void onPause() {
        // STUB
    }

    @Override
    public void addDownloadObserver(DownloadObserver observer) {
        // STUB
    }

    @Override
    public void removeDownloadObserver(DownloadObserver observer) {
        // STUB
    }

    @Override
    public void downloadDash(String url, String pid, String metadata, long expirationDate, AddToQueueListener listener) {
        // STUB
    }


    @Override
    public void deleteAsset(String assetId) {
        // STUB
    }

    @Override
    public boolean isExpired(String assetId) {
        // STUB
        return false;
    }

    @Override
    public boolean isDiskCapacityOk() {
        // STUB
        return true;
    }

    @Override
    public boolean isDownloadLimitOk() {
        // STUB
        return true;
    }

    @Override
    public boolean isNetworkStatusOk() {
        // STUB
        return true;
    }

    @Override
    public boolean isPowerStatusOK() {
        // STUB
        return true;
    }

    @Override
    public boolean isDownloadAvailable() {
        return false;
    }
}
