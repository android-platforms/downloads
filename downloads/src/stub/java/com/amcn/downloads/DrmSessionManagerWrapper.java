package com.amcn.downloads;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;

import com.google.android.exoplayer2.drm.DrmInitData;
import com.google.android.exoplayer2.drm.DrmSession;
import com.google.android.exoplayer2.drm.DrmSessionManager;
import com.google.android.exoplayer2.drm.FrameworkMediaCrypto;

import java.util.HashMap;
import java.util.UUID;

/**
 * Stub implementation of DrmSessionManager
 */
public class DrmSessionManagerWrapper implements DrmSessionManager<FrameworkMediaCrypto> {

    public DrmSessionManagerWrapper(Context context,
                                    UUID uuid,
                                    String remoteAssetId,
                                    HashMap<String, String> optionalKeyRequestParameters,
                                    Looper playbackLooper,
                                    Handler eventHandler) {
    }

    @Override
    public boolean canAcquireSession(DrmInitData drmInitData) {
        return true;
    }

    @Override
    public DrmSession<FrameworkMediaCrypto> acquireSession(Looper playbackLooper, final DrmInitData drmInitData) {
        return null;
    }

    @Override
    public void releaseSession(DrmSession<FrameworkMediaCrypto> drmSession) {
    }
}