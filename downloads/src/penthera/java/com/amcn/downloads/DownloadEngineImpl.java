package com.amcn.downloads;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.util.Log;

import com.amcn.downloads.listeners.AddToQueueListener;
import com.amcn.downloads.model.DownloadingItem;
import com.penthera.VirtuosoSDK;
import com.penthera.virtuososdk.Common;
import com.penthera.virtuososdk.Common.AssetStatus;
import com.penthera.virtuososdk.client.*;
import com.penthera.virtuososdk.client.database.AssetColumns;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

class DownloadEngineImpl extends DownloadEngine {

    private static final String TAG = "DownloadEngine";
    private static final int DESIRED_BITRATE = 1200000; // 540p (sd)
    private static final float BATTERY_THRESHOLD = 0.2f;

    private Virtuoso virtuoso;
    private DownloadingCache downloadingCache = new DownloadingCache();
    private final DownloadObservable downloadObservable = new DownloadObservable();

    DownloadEngineImpl(Context applicationContext) {
        virtuoso = new Virtuoso(applicationContext);
        updateEngineSettings();
        initDownloadingCache();
        Log.i(TAG, "Virtuoso SDK version: " + VirtuosoSDK.FULL_VERSION);
    }

    @Override
    public void startEngine(URL url, String userId, String publicKey, String privateKey) {
        virtuoso.startup(url, userId, null, publicKey, privateKey, registrationObserver);
    }

    private void updateEngineSettings() {
        if (isAuthorized()) {
            virtuoso.getSettings().setBatteryThreshold(BATTERY_THRESHOLD).save();
        }
    }

    @Override
    public void stopEngine() {
        try {
            virtuoso.getBackplane().unregister();
        } catch (BackplaneException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean isAuthorized() {
        return virtuoso.getBackplane().getAuthenticationStatus() == Common.AuthenticationStatus.AUTHENTICATED;
    }

    @Override
    public Uri getContentURI() {
        return virtuoso.getAssetManager().CONTENT_URI();
    }

    @Override
    public Uri getExpiredURI() {
        return virtuoso.getAssetManager().getExpired().CONTENT_URI();
    }

    @Override
    public Uri getQueueURI() {
        return virtuoso.getAssetManager().getQueue().CONTENT_URI();
    }

    @Override
    public @DownloadStatus
    int getAssetDownloadStatus(String assetId) {
        int result = DownloadStatus.NOT_EXISTS;
        DownloadingItem downloadingItem = downloadingCache.getAssetStatus(assetId);
        if (downloadingItem != null) {
            result = downloadingItem.getDownloadStatus();
        } else {
            List<IIdentifier> assets = virtuoso.getAssetManager().getByAssetId(assetId);
            if (assets.size() > 0) {
                ISegmentedAsset asset = ((ISegmentedAsset) assets.get(0));
                int status = asset.getDownloadStatus();
                if (status == AssetStatus.DOWNLOAD_COMPLETE && getUriFromAsset(asset) == null) {
                    result = DownloadStatus.ERROR;
                } else {
                    result = mapAssetStatusToDownloadStatus(status);
                }
            }
        }

        // lets check if we are ok to proceed with downloading
        if (result == DownloadStatus.DOWNLOADING) {
            return (isDiskCapacityOk() && isDownloadLimitOk() && isNetworkStatusOk() && isPowerStatusOK())
                    ? result : DownloadStatus.ERROR;
        }
        return result;
    }

    private int mapAssetStatusToDownloadStatus(int status) {
        int result = DownloadStatus.NOT_EXISTS;

        switch (status) {
            case AssetStatus.DOWNLOAD_PENDING:
            case AssetStatus.DOWNLOADING:
                result = DownloadStatus.DOWNLOADING;
                break;
            case AssetStatus.DOWNLOAD_COMPLETE:
                result = DownloadStatus.DOWNLOADED;
                break;
            case AssetStatus.DOWNLOAD_NETWORK_ERROR:
            case AssetStatus.DOWNLOAD_REACHABILITY_ERROR:
            case AssetStatus.DOWNLOAD_FILE_COPY_ERROR:
            case AssetStatus.DOWNLOAD_FILE_MIME_MISMATCH:
            case AssetStatus.DOWNLOAD_FILE_SIZE_MISMATCH:
            case AssetStatus.EXPIRED:
            case AssetStatus.DOWNLOAD_DENIED_MAX_DEVICE_DOWNLOADS:
            case AssetStatus.DOWNLOAD_DENIED_ACCOUNT:
            case AssetStatus.DOWNLOAD_DENIED_ASSET:
            case AssetStatus.DOWNLOAD_BLOCKED_AWAITING_PERMISSION:
                result = DownloadStatus.ERROR;
                break;
        }

        return result;
    }

    @Override
    public DownloadingItem getDownloadingItem(String assetId) {
        return downloadingCache.getAssetStatus(assetId);
    }

    @Override
    public int getDownloadingQueueSize() {
        return downloadingCache.getTotalCount();
    }

    @Override
    public int getDownloadingFinishedCount() {
        return downloadingCache.getFinishedCount();
    }

    @Override
    public DownloadingItem getCurrentDownloadingItem() {
        return downloadingCache.getCurrentDownloading();
    }

    @Override
    public String getAssetUri(String assetId) {
        String uri = null;
        List<IIdentifier> assets = virtuoso.getAssetManager().getByAssetId(assetId);
        if (assets.size() > 0) {
            ISegmentedAsset sa = (ISegmentedAsset) assets.get(0);
            uri = getUriFromAsset(sa);
        }

        return uri;
    }

    private String getUriFromAsset(ISegmentedAsset asset) {
        String uri = null;
        try {
            if (asset.getPlaylist() != null) {
                uri = asset.getPlaylist().toString();
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        return uri;
    }

    private void invalidateDownloadingCache() {
        List<DownloadingItem> cache = new ArrayList<>(downloadingCache.getCache());
        for (DownloadingItem item : cache) {
            if (isDownloaded(item.getAssetId())) {
                downloadingCache.removeAsset(item.getAssetId());
            }
        }
    }

    private boolean isDownloaded(String assetId) {
        Cursor c = null;
        try {
            c = virtuoso.getAssetManager().getDownloaded().getCursor(new String[]{AssetColumns._ID}, AssetColumns.ASSET_ID + "=?", new String[]{assetId});
            return c != null && c.getCount() > 0;
        } finally {
            if (c != null && !c.isClosed()) {
                c.close();
            }
        }
    }

    @Override
    public void onResume() {
        virtuoso.onResume();
        invalidateDownloadingCache();
        virtuoso.addObserver(backplaneObserver);
        virtuoso.addObserver(queueObserver);
    }

    @Override
    public void onPause() {
        virtuoso.removeObserver(backplaneObserver);
        virtuoso.removeObserver(queueObserver);
        virtuoso.onPause();
    }

    @Override
    public void addDownloadObserver(DownloadObserver observer) {
        downloadObservable.registerObserver(observer);
    }

    @Override
    public void removeDownloadObserver(DownloadObserver observer) {
        downloadObservable.unregisterObserver(observer);
    }

    @Override
    public void downloadDash(String url, String pid, String metadata, long expirationDate, AddToQueueListener listener) {
        ISegmentedAssetFromParserObserver observer = new ISegmentedAssetFromParserObserver() {

            @Override
            public void complete(ISegmentedAsset aSegmentedAsset, int aError, boolean addedToQueue) {
                if (aSegmentedAsset != null) {
                    DownloadingItem item = createDownloadItem(aSegmentedAsset);
                    item.setDownloadStatus(DownloadStatus.DOWNLOADING);
                    downloadingCache.updateAssetStatus(item);

                    downloadObservable.onDownloadStarted(aSegmentedAsset.getAssetId());
                    if (listener != null) {
                        listener.onSuccess(pid);
                    }
                } else {
                    if (listener != null) {
                        listener.onFailure(pid);
                    }
                }
            }

            @Override
            public String didParseSegment(ISegment segment) {
                return segment.getRemotePath();
            }

            @Override
            public void willAddToQueue(ISegmentedAsset iSegmentedAsset) {
                iSegmentedAsset.setEndWindow(expirationDate);
                virtuoso.getAssetManager().update(iSegmentedAsset);
            }
        };
        try {
            virtuoso.getAssetManager().createMPDSegmentedAssetAsync(observer, new URL(url), DESIRED_BITRATE, 0, pid, metadata, true, null);
        } catch (MalformedURLException e) {
            if (listener != null) {
                listener.onFailure(pid);
            }
        }
    }

    @Override
    public void deleteAsset(String assetId) {
        List<IIdentifier> assets = virtuoso.getAssetManager().getByAssetId(assetId);
        for (IIdentifier asset : assets) {
            virtuoso.getAssetManager().delete((IAsset) asset);
        }
        downloadingCache.removeAsset(assetId);
        downloadObservable.onDownloadDeleted(assetId);
    }

    @Override
    public boolean isExpired(String assetId) {
        List<IIdentifier> assets = virtuoso.getAssetManager().getByAssetId(assetId);
        if (assets.size() > 0) {
            ISegmentedAsset asset = ((ISegmentedAsset) assets.get(0));
            int status = ((ISegmentedAsset) assets.get(0)).getDownloadStatus();
            if (status == AssetStatus.DOWNLOAD_COMPLETE && getUriFromAsset(asset) == null) {
                return true;
            } else {
                return status == AssetStatus.EXPIRED;
            }
        }
        return false;
    }

    @Override
    public boolean isDiskCapacityOk() {
        return virtuoso.isDiskStatusOK();
    }

    @Override
    public boolean isDownloadLimitOk() {
        return virtuoso.isDownloadLimitOK();
    }

    @Override
    public boolean isNetworkStatusOk() {
        return virtuoso.isNetworkStatusOK();
    }

    @Override
    public boolean isPowerStatusOK() {
        return virtuoso.isPowerStatusOK();
    }

    @Override
    public boolean isDownloadAvailable() {
        return true;
    }

    private Observers.IBackplaneObserver backplaneObserver = new Observers.IBackplaneObserver() {
        @Override
        public void requestComplete(int request, int result) {
            updateEngineSettings();
            Log.i(TAG, "backplane - " + request + " - " + result);
        }
    };


    private IPushRegistrationObserver registrationObserver = new IPushRegistrationObserver() {
        @Override
        public void onServiceAvailabilityResponse(int pushService, int errorCode) {
            Log.i(TAG, pushService + " - " + errorCode);
        }
    };

    private DownloadingItem createDownloadItem(ISegmentedAsset asset) {
        DownloadingItem item = new DownloadingItem();
        item.setAssetId(asset.getAssetId());
        item.setCurrentSize((long) asset.getCurrentSize());
        item.setTotalSize((long) asset.getExpectedSize());
        item.setMetadata(asset.getMetadata());

        return item;
    }

    private void initDownloadingCache() {
        Cursor c = virtuoso.getAssetManager().getQueue().getCursor();
        while (c.moveToNext()) {
            String assetId = c.getString(c.getColumnIndex(AssetColumns.ASSET_ID));
            List<IIdentifier> assets = virtuoso.getAssetManager().getByAssetId(assetId);
            if (assets.size() > 0) {
                ISegmentedAsset iSegmentedAsset = (ISegmentedAsset) assets.get(0);
                DownloadingItem downloadingItem = createDownloadItem(iSegmentedAsset);
                downloadingItem.setDownloadStatus(mapAssetStatusToDownloadStatus(iSegmentedAsset.getDownloadStatus()));
                downloadingCache.updateAssetStatus(downloadingItem);
            }
        }
    }

    private QueueObserver queueObserver = new QueueObserver() {
        @Override
        public void engineStartedDownloadingAsset(IIdentifier aAsset) {
            ISegmentedAsset iSegmentedAsset = (ISegmentedAsset) aAsset;

            DownloadingItem item = createDownloadItem(iSegmentedAsset);
            item.setDownloadStatus(mapAssetStatusToDownloadStatus(iSegmentedAsset.getDownloadStatus()));
            downloadingCache.updateAssetStatus(item);

            String assetId = iSegmentedAsset.getAssetId();
            downloadObservable.onDownloadStarted(assetId);
        }

        @Override
        public void enginePerformedProgressUpdateDuringDownload(IIdentifier aAsset) {
            ISegmentedAsset iSegmentedAsset = (ISegmentedAsset) aAsset;

            DownloadingItem item = createDownloadItem(iSegmentedAsset);
            item.setDownloadStatus(mapAssetStatusToDownloadStatus(iSegmentedAsset.getDownloadStatus()));
            downloadingCache.updateAssetStatus(item);

            String assetId = iSegmentedAsset.getAssetId();
            long currentSize = (long) iSegmentedAsset.getCurrentSize();
            long totalSize = (long) iSegmentedAsset.getExpectedSize();
            downloadObservable.onDownloadProgressUpdate(assetId, currentSize, totalSize);
        }

        @Override
        public void engineCompletedDownloadingAsset(IIdentifier aAsset) {
            ISegmentedAsset iSegmentedAsset = (ISegmentedAsset) aAsset;

            DownloadingItem item = createDownloadItem(iSegmentedAsset);
            item.setDownloadStatus(mapAssetStatusToDownloadStatus(iSegmentedAsset.getDownloadStatus()));
            downloadingCache.updateAssetStatus(item);

            String assetId = iSegmentedAsset.getAssetId();
            downloadObservable.onDownloadCompleted(assetId);
        }

        @Override
        public void engineEncounteredErrorDownloadingAsset(IIdentifier aAsset) {
            ISegmentedAsset iSegmentedAsset = (ISegmentedAsset) aAsset;

            DownloadingItem item = createDownloadItem(iSegmentedAsset);
            item.setDownloadStatus(mapAssetStatusToDownloadStatus(iSegmentedAsset.getDownloadStatus()));
            downloadingCache.updateAssetStatus(item);

            String assetId = iSegmentedAsset.getAssetId();
            downloadObservable.onDownloadError(assetId);
        }
    };
}
