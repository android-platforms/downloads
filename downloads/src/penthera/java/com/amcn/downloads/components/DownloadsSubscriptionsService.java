package com.amcn.downloads.components;

import com.penthera.virtuososdk.client.subscriptions.SubscriptionsService;

public class DownloadsSubscriptionsService extends SubscriptionsService {

    public DownloadsSubscriptionsService(String name) {
        super(name);
    }

    public DownloadsSubscriptionsService() {
        super("DownloadsGcmService");
    }

}
