package com.amcn.downloads.components;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import androidx.core.app.NotificationCompat;

import com.amcn.downloads.R;
import com.penthera.virtuososdk.service.VirtuosoServiceStarter;

public class DownloadsServiceStarter extends VirtuosoServiceStarter {
    private static String foregroundServiceNotificationAction = "com.amcn.downloads.action.ForegroundDownloadNotificationAction";

    private static NotificationChannel notificationChannel = null;
    private static Notification currentNotification = null;

    private final static String CHANNEL_ID = "VIRTUOSO_CHANNEL_ID";
    private final static String CHANNEL_NAME = "Default";

    public static void updateNotification(Context aContext, Intent aIntent) {
        updateNotification(aContext, aIntent, DownloadsServiceStarter.class);
    }

    @Override
    protected Notification getForegroundServiceNotification(Context context, Intent forIntent) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O && notificationChannel == null) {
            notificationChannel = new NotificationChannel(CHANNEL_ID, CHANNEL_NAME, NotificationManager.IMPORTANCE_LOW);
            notificationChannel.enableLights(false);
            notificationChannel.enableVibration(false);
            NotificationManager manager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            if (manager != null) {
                manager.createNotificationChannel(notificationChannel);
            }
        }

        Intent notificationIntent = new Intent(foregroundServiceNotificationAction);

        PendingIntent pIntent = PendingIntent.getActivity(context, 0, notificationIntent, PendingIntent.FLAG_CANCEL_CURRENT);
        Notification notification = new NotificationCompat.Builder(context, CHANNEL_ID)
                .setContentIntent(pIntent)
                .setContentTitle(context.getString(R.string.download_notification_title))
                .setSmallIcon(R.drawable.ic_file_download)
                .build();
        if (currentNotification != null && notification != null) {
            currentNotification.contentView = notification.contentView;
        } else if (notification != null) {
            currentNotification = notification;
        }

        return currentNotification;
    }
}
