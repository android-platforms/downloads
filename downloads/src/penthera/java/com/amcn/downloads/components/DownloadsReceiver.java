package com.amcn.downloads.components;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class DownloadsReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        DownloadsServiceStarter.updateNotification(context, intent);
    }
}
