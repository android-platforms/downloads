package com.amcn.downloads.components;

import com.penthera.virtuososdk.database.impl.provider.VirtuosoSDKContentProvider;

public class DownloadsContentProvider extends VirtuosoSDKContentProvider {

    static {
        setAuthority("com.amcn.downloads.virtuoso.content.provider");
    }

    @Override
    protected String getAuthority() {
        return "com.amcn.downloads.virtuoso.content.provider";
    }
}
