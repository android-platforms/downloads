package com.amcn.downloads.model;

import com.penthera.virtuososdk.client.database.AssetColumns;

class AssetColumnsImpl {

    static final String _ID = AssetColumns._ID;
    static final String DOWNLOAD_STATUS = AssetColumns.DOWNLOAD_STATUS;
    static final String METADATA = AssetColumns.METADATA;
    static final String CURRENT_SIZE = AssetColumns.CURRENT_SIZE;
    static final String EXPECTED_SIZE = AssetColumns.EXPECTED_SIZE;
    static final String CREATION_TIME = AssetColumns.CREATION_TIME;

    private AssetColumnsImpl() {
    }
}
