package com.amcn.downloads;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import androidx.annotation.NonNull;
import android.util.Log;

import com.google.android.exoplayer2.drm.DrmInitData;
import com.google.android.exoplayer2.drm.DrmSession;
import com.google.android.exoplayer2.drm.DrmSessionManager;
import com.google.android.exoplayer2.drm.FrameworkMediaCrypto;
import com.penthera.virtuososdk.client.drm.IDrmInitData;
import com.penthera.virtuososdk.client.drm.IVirtuosoDrmSession;
import com.penthera.virtuososdk.client.drm.VirtuosoDrmSessionManager;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * Wraps the VirtuosoDrmSessionManager
 */

@SuppressWarnings("unused")
public class DrmSessionManagerWrapper implements DrmSessionManager<FrameworkMediaCrypto> {
    private static final String TAG = DrmSessionManagerWrapper.class.getSimpleName();

    private VirtuosoDrmSessionManager mDrmSessionManager;

    private DrmSessionManagerWrapper(Context context,
                                    UUID uuid,
                                    String remoteAssetId,
                                    HashMap<String, String> optionalKeyRequestParameters,
                                    Looper playbackLooper,
                                    Handler eventHandler) {
        try {
            mDrmSessionManager = new VirtuosoDrmSessionManager(context, uuid, remoteAssetId, optionalKeyRequestParameters,
                    playbackLooper, eventHandler, new VirtuosoDrmSessionManager.EventListener() {
                @Override
                public void onDrmKeysLoaded() {
                    Log.d(TAG, "onDrmKeysLoaded");
                }

                @Override
                public void onDrmSessionManagerError(Exception e) {
                    e.printStackTrace();
                }
            });
        } catch (com.penthera.virtuososdk.client.drm.UnsupportedDrmException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean canAcquireSession(final DrmInitData drmInitData) {
        return mDrmSessionManager.canOpen(schemeUuid -> {
            DrmInitData.SchemeData sd = drmInitData.get(schemeUuid);
            return new IDrmInitData.SchemeInitData(sd.mimeType, sd.data);
        });
    }

    @Override
    public DrmSession<FrameworkMediaCrypto> acquireSession(Looper playbackLooper, final DrmInitData drmInitData) {
        IVirtuosoDrmSession session = mDrmSessionManager.open(schemeUuid -> {
            DrmInitData.SchemeData sd = drmInitData.get(schemeUuid);
            return new IDrmInitData.SchemeInitData(sd.mimeType, sd.data);
        });
        session.setLooper(playbackLooper);

        return new DrmSessionWrapper(session);
    }

    @Override
    public void releaseSession(DrmSession<FrameworkMediaCrypto> drmSession) {
        DrmSessionWrapper sessionWrapper = (DrmSessionWrapper) drmSession;
        mDrmSessionManager.close(sessionWrapper.getVirtuosoSession());
    }

    static class DrmSessionWrapper implements DrmSession<FrameworkMediaCrypto> {

        private final IVirtuosoDrmSession drmSession;
        private FrameworkMediaCrypto mediaCrypto = null;

        @SuppressWarnings("unused")
        DrmSessionWrapper(@NonNull IVirtuosoDrmSession session) {
            drmSession = session;
            mediaCrypto = new FrameworkMediaCrypto(drmSession.getMediaCrypto());
        }

        IVirtuosoDrmSession getVirtuosoSession() {
            return drmSession;
        }

        @Override
        public int getState() {
            return drmSession.getState();
        }

        @Override
        public FrameworkMediaCrypto getMediaCrypto() {
            return mediaCrypto;
        }

        @Override
        public DrmSessionException getError() {
            Exception e = drmSession.getError();
            if (e != null) {
                return new DrmSessionException(e);
            }
            return null;
        }

        @Override
        public Map<String, String> queryKeyStatus() {
            return drmSession.queryKeyStatus();
        }

        @Override
        public byte[] getOfflineLicenseKeySetId() {
            return drmSession.getLicenseKeySetId();
        }
    }
}