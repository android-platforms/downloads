package com.amcn.downloads.drm;

import android.text.TextUtils;

import com.penthera.virtuososdk.client.drm.LicenseManager;

import org.json.JSONException;
import org.json.JSONObject;

class DownloadsLicenseManager extends LicenseManager {

    public DownloadsLicenseManager() {
        super();
    }

    @Override
    public String getLicenseAcquistionUrl() {
        String licenseUrl = null;
        if (mAsset != null && !TextUtils.isEmpty(mAsset.getMetadata())) {
            try {
                JSONObject jsonObject = new JSONObject(mAsset.getMetadata());
                licenseUrl = jsonObject.optString("license_url");
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return licenseUrl;
    }
}